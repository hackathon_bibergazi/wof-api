package com.wofapi.repository;

import com.wofapi.domain.WOFDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface WOFRepository extends JpaRepository<WOFDetail, Long> {

    Optional<WOFDetail> findTopByUserIdOrderByCreatedDateDesc(Long userId);

}

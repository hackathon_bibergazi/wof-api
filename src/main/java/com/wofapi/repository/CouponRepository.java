package com.wofapi.repository;

import com.wofapi.domain.Coupon;
import com.wofapi.exception.CouponNotFoundException;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface CouponRepository extends JpaRepository<Coupon, Long> {

    Optional<Coupon> findByIdAndActivated(Long id, boolean activated);

    List<Coupon> findByActivatedOrderByIdAsc(boolean activated);

    default List<Coupon> findActivatedCoupons() {
        return findByActivatedOrderByIdAsc(true);
    }

    default Coupon findActivatedOne(Long id) {
        return findByIdAndActivated(id, true).orElseThrow(() -> new CouponNotFoundException(id + " id activate coupon not found"));
    }

    default Coupon findDeactivatedOne(Long id) {
        return findByIdAndActivated(id, false).orElseThrow(() -> new CouponNotFoundException(id + " id deactivated coupon not found"));
    }

    default Coupon findOne(Long id) {
        return findById(id).orElseThrow(() -> new CouponNotFoundException(id + " id coupon not found"));
    }

}

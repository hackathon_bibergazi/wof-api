package com.wofapi.configuration;

import com.google.common.base.Predicates;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.spi.DocumentationType.SWAGGER_2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {

    @Bean
    public Docket questionAnswerApi() {
        return new Docket(SWAGGER_2)
                .apiInfo(new ApiInfo(
                        "WOF API",
                        "WOF API Endpoints",
                        "0.0.1",
                        null,
                        new Contact("Biber Gazı Team", null, "bibergaziteam@trendyol.com"),
                        null,
                        null,
                        Collections.emptyList()))
                .useDefaultResponseMessages(false)
                .select()
                .paths(Predicates.not(PathSelectors.regex("/error.*")))
                .build();
    }

}

package com.wofapi.domain;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wofapi.common.Capability;

import javax.persistence.*;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

@Entity
@Table(name = "coupon",
        uniqueConstraints = @UniqueConstraint(columnNames = {"discount", "lower_limit"})
)
@SequenceGenerator(name = "SEQ", sequenceName = "coupon_id_seq", allocationSize = 1)
public class Coupon extends BaseEntity {

    @Column(name = "discount", nullable = false)
    private BigDecimal discount;

    @Column(name = "probability", nullable = false)
    private Integer probability;

    @Column(name = "lower_limit", nullable = false)
    private BigDecimal lowerLimit;

    @Column(name = "capability", nullable = false)
    private String capability = Capability.ALL.name();

    @Column(columnDefinition = "TEXT")
    private String descriptions = "[]";

    @Column(name = "activated", nullable = false)
    private boolean activated = true;

    public static Coupon createCoupon(BigDecimal discount, Integer probability, BigDecimal lowerLimit) {
        return new Coupon(discount, probability, lowerLimit);
    }

    public void deactivate() {
        this.activated = false;
    }

    public void activated() {
        this.activated = true;
    }

    public List<String> getDescriptions() {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            return objectMapper.readValue(this.descriptions,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, String.class));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }

    public void addDescriptions(List<String> descriptions) {
        List<String> persistedDescriptions = getDescriptions();
        persistedDescriptions.addAll(descriptions);

        try {
            this.descriptions = new ObjectMapper().writeValueAsString(persistedDescriptions);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public void removeDescriptions(List<String> descriptions) {
        List<String> persistedDescriptions = getDescriptions();
        persistedDescriptions.removeAll(descriptions);

        try {
            this.descriptions = new ObjectMapper().writeValueAsString(persistedDescriptions);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public Coupon() {
    }

    public Coupon(BigDecimal discount, Integer probability, BigDecimal lowerLimit) {
        this.discount = discount;
        this.probability = probability;
        this.lowerLimit = lowerLimit;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Integer getProbability() {
        return probability;
    }

    public void setProbability(Integer probability) {
        this.probability = probability;
    }

    public BigDecimal getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(BigDecimal lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public String getCapability() {
        return capability;
    }

    public void setCapability(String capability) {
        this.capability = capability;
    }

    public void setDescriptions(String descriptions) {
        this.descriptions = descriptions;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

}

package com.wofapi.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@Table(name = "wof_detail")
@SequenceGenerator(name = "SEQ", sequenceName = "wof_detail_id_seq", allocationSize = 1)
public class WOFDetail extends BaseEntity {

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Column(name = "spun", nullable = false)
    private boolean spun = true;

    @Column(name = "expired_date")
    private Instant expiredDate;

    @Column(name = "coupon_id", nullable = false)
    private Long couponId;

    @Column(name = "persisted", nullable = false)
    private boolean persisted = false;

    public static WOFDetail createPersistedCouponWOFDetail(Long userId, Instant expiredDate, Long couponId) {
        return new WOFDetail(userId, expiredDate, couponId, true);
    }

    public static WOFDetail createNotPersistedCouponWOFDetail(Long userId, Instant expiredDate, Long couponId) {
        return new WOFDetail(userId, expiredDate, couponId);
    }

    public WOFDetail() {
    }

    public WOFDetail(Long userId, Instant expiredDate, Long couponId) {
        this.userId = userId;
        this.expiredDate = expiredDate;
        this.couponId = couponId;
    }

    public WOFDetail(Long userId, Instant expiredDate, Long couponId, boolean persisted) {
        this.userId = userId;
        this.expiredDate = expiredDate;
        this.couponId = couponId;
        this.persisted = persisted;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isSpun() {
        return spun;
    }

    public void setSpun(boolean spun) {
        this.spun = spun;
    }

    public Instant getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(Instant expiredDate) {
        this.expiredDate = expiredDate;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public boolean isPersisted() {
        return persisted;
    }

    public void setPersisted(boolean persisted) {
        this.persisted = persisted;
    }

}

package com.wofapi.controller;

import com.wofapi.dto.CouponDTO;
import com.wofapi.dto.UserSpinTransactionDTO;
import com.wofapi.dto.request.WOFDetailCreateRequest;
import com.wofapi.service.WOFDetailService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/wof-details")
public class WOFDetailController {

    private final WOFDetailService wofDetailService;

    public WOFDetailController(WOFDetailService wofDetailService) {
        this.wofDetailService = wofDetailService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createWOFDetail(@Valid @RequestBody WOFDetailCreateRequest request) {
        wofDetailService.createWOFDetail(request);
    }

    @PostMapping("/spin")
    @ResponseStatus(HttpStatus.OK)
    public CouponDTO spinWOF() {
        return wofDetailService.spinWOF();
    }

    @GetMapping("/spin/{userId}")
    @ResponseStatus(HttpStatus.OK)
    public UserSpinTransactionDTO userLastSpinTransaction(@PathVariable Long userId) {
        return wofDetailService.userLastSpinTransaction(userId);
    }

}

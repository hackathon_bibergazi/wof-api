package com.wofapi.controller;

import com.wofapi.dto.CouponDTO;
import com.wofapi.dto.request.CouponCreateRequest;
import com.wofapi.dto.request.DescriptionsAddRequest;
import com.wofapi.dto.request.DescriptionsRemoveRequest;
import com.wofapi.service.CouponService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/coupons")
public class CouponController {

    private final CouponService couponService;

    public CouponController(CouponService couponService) {
        this.couponService = couponService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void createCoupon(@Valid @RequestBody CouponCreateRequest request) {
        couponService.createCoupon(request);
    }

    @GetMapping("/available")
    @ResponseStatus(HttpStatus.OK)
    public List<CouponDTO> getAvailableCoupons() {
        return couponService.getAvailableCoupons();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void deactivateAvailableCoupon(@PathVariable(name = "id") Long couponId) {
        couponService.deactivateAvailableCoupon(couponId);
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public void activateAvailableCoupon(@PathVariable(name = "id") Long couponId) {
        couponService.activateAvailableCoupon(couponId);
    }

    @PostMapping("/descriptions")
    @ResponseStatus(HttpStatus.OK)
    public void addDescriptions(@RequestBody DescriptionsAddRequest request) {
        couponService.addDescriptions(request);
    }

    @DeleteMapping("/descriptions")
    @ResponseStatus(HttpStatus.OK)
    public void deleteDescriptions(@RequestBody DescriptionsRemoveRequest request) {
        couponService.removeDescriptions(request);
    }

}

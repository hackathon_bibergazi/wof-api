package com.wofapi.controller;

import com.wofapi.service.HealthService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/")
public class IndexController {

    private final HealthService healthService;

    public IndexController(HealthService healthService) {
        this.healthService = healthService;
    }

    @GetMapping
    public RedirectView redirectToSwaggerUi(HttpServletResponse response) {
        response.setHeader("Cache-Control", "no-cache");
        return new RedirectView("/swagger-ui.html");
    }

    @GetMapping("unhealthy")
    public void unhealthly() throws InterruptedException {
        healthService.unhealthy();
    }

}

package com.wofapi.controller;

import com.wofapi.dto.UserResponseDto;
import com.wofapi.service.BackendUserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/users")
public class UserController {

    private final BackendUserService backendUserService;

    public UserController(BackendUserService backendUserService) {
        this.backendUserService = backendUserService;
    }

    @GetMapping("/email/{email}")
    @ResponseStatus(HttpStatus.OK)
    public UserResponseDto getUserByEmail(@PathVariable String email) {
        return backendUserService.getUserByEmail(email);
    }
}

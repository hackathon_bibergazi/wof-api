package com.wofapi.handler;

import com.wofapi.exception.GenericRestClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.net.URI;

import static org.springframework.http.HttpStatus.Series.INFORMATIONAL;
import static org.springframework.http.HttpStatus.Series.SUCCESSFUL;

public class RestTemplateResponseErrorHandler implements ResponseErrorHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(RestTemplateResponseErrorHandler.class);

    @Override
    public boolean hasError(ClientHttpResponse response) throws IOException {
        HttpStatus.Series series = response.getStatusCode().series();
        return series != INFORMATIONAL && series != SUCCESSFUL;
    }

    @Override
    public void handleError(ClientHttpResponse response) throws IOException {
        throw new GenericRestClientException(response.getStatusText(), response.getStatusCode());
    }

    @Override
    public void handleError(URI url, HttpMethod method, ClientHttpResponse response) throws IOException {
        LOGGER.warn("API call failed with the url: {}, status code: {} and message: {}", url, response.getStatusCode(), response.getStatusText());
        handleError(response);
    }

}
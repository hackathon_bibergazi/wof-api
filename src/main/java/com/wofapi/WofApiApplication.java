package com.wofapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WofApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WofApiApplication.class, args);
	}

}

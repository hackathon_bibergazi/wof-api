package com.wofapi.common;

import com.wofapi.dto.CouponDTO;

import java.util.List;
import java.util.NavigableMap;
import java.util.Random;
import java.util.TreeMap;

public class RandomCouponCollection {

    private final NavigableMap<Double, CouponDTO> couponMap = new TreeMap();
    private final Random random = new Random();
    private double totalProbability = 0;

    public void add(List<CouponDTO> couponDTOS) {
        couponDTOS.forEach(this::add);
    }

    public void add(CouponDTO couponDTO) {
        if (couponDTO.getProbability() <= 0) return;
        totalProbability += couponDTO.getProbability();
        couponMap.put(totalProbability, couponDTO);
    }

    public CouponDTO next() {
        Double value = random.nextDouble() * totalProbability;
        return couponMap.higherEntry(value).getValue();
    }

}

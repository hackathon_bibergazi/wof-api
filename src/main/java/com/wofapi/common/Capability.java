package com.wofapi.common;

public enum Capability {
    ALL, BOUTIQUE, CATEGORY, TAB, PRODUCT
}

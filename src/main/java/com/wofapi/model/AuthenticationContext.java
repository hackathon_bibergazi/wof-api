package com.wofapi.model;

import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class AuthenticationContext {

    private String accessToken;
    private String type;
    private Date expireDate;

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }
}

package com.wofapi.dto;

import java.math.BigDecimal;
import java.util.List;

public class CouponDTO {

    private Long id;
    private BigDecimal discount;
    private Integer probability;
    private BigDecimal lowerLimit;
    private String capability;
    private List<String> descriptions;
    private boolean activated;

    public CouponDTO(Long id,
                     BigDecimal discount,
                     Integer probability,
                     BigDecimal lowerLimit,
                     String capability,
                     List<String> descriptions,
                     boolean activated) {
        this.id = id;
        this.discount = discount;
        this.probability = probability;
        this.lowerLimit = lowerLimit;
        this.capability = capability;
        this.descriptions = descriptions;
        this.activated = activated;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Integer getProbability() {
        return probability;
    }

    public void setProbability(Integer probability) {
        this.probability = probability;
    }

    public BigDecimal getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(BigDecimal lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public String getCapability() {
        return capability;
    }

    public void setCapability(String capability) {
        this.capability = capability;
    }

    public List<String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<String> descriptions) {
        this.descriptions = descriptions;
    }

    public boolean isActivated() {
        return activated;
    }

    public void setActivated(boolean activated) {
        this.activated = activated;
    }

}

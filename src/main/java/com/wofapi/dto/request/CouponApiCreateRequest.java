package com.wofapi.dto.request;

import com.wofapi.dto.CouponCondition;

import java.math.BigDecimal;
import java.time.Instant;

public class CouponApiCreateRequest {

    private CouponCondition condition;
    private String createdBy;
    private BigDecimal discount;
    private Instant endDate;
    private Instant startDate;
    private Integer typeId;
    private Long userId;

    public CouponCondition getCondition() {
        return condition;
    }

    public void setCondition(CouponCondition condition) {
        this.condition = condition;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Instant getEndDate() {
        return endDate;
    }

    public void setEndDate(Instant endDate) {
        this.endDate = endDate;
    }

    public Instant getStartDate() {
        return startDate;
    }

    public void setStartDate(Instant startDate) {
        this.startDate = startDate;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setTypeId(Integer typeId) {
        this.typeId = typeId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "CouponApiCreateRequest{" +
                "condition=" + condition +
                ", createdBy='" + createdBy + '\'' +
                ", discount=" + discount +
                ", endDate=" + endDate +
                ", startDate=" + startDate +
                ", typeId=" + typeId +
                ", userId=" + userId +
                '}';
    }
}

package com.wofapi.dto.request;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

public class DescriptionsAddRequest {

    @NotNull(message = "coupon id must not be null or empty")
    private Long couponId;

    @NotEmpty(message = "description list must not be empty")
    private List<String> descriptions = new ArrayList<>();

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public List<String> getDescriptions() {
        return descriptions;
    }

    public void setDescriptions(List<String> descriptions) {
        this.descriptions = descriptions;
    }

}

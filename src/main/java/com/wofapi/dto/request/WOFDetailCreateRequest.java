package com.wofapi.dto.request;

import javax.validation.constraints.NotNull;

public class WOFDetailCreateRequest {

    @NotNull(message = "user id must not be null or empty")
    private Long userId;

    @NotNull(message = "coupon id must not be null or empty")
    private Long couponId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }
}

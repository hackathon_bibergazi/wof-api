package com.wofapi.dto.request;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.math.BigDecimal;

public class CouponCreateRequest {

    @Min(value = 0, message = "discount value must be greater than zero")
    private BigDecimal discount = BigDecimal.ZERO;

    @Min(value = 0, message = "probability value must be greater than zero")
    @Max(value = 100, message = "probability value must be less than or equal to a hundred")
    private Integer probability = 0;

    @Min(value = 0, message = "lower limit value must be greater than zero")
    private BigDecimal lowerLimit = BigDecimal.ZERO;

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public Integer getProbability() {
        return probability;
    }

    public void setProbability(Integer probability) {
        this.probability = probability;
    }

    public BigDecimal getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(BigDecimal lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

}

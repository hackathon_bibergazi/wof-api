package com.wofapi.dto.request;

import com.wofapi.dto.CouponCondition;

import java.math.BigDecimal;
import java.time.Instant;

public final class CouponApiCreateRequestBuilder {
    private CouponCondition condition;
    private String createdBy;
    private BigDecimal discount;
    private Instant endDate;
    private Instant startDate;
    private Integer typeId;
    private Long userId;

    private CouponApiCreateRequestBuilder() {
    }

    public static CouponApiCreateRequestBuilder couponApiCreateRequest() {
        return new CouponApiCreateRequestBuilder();
    }

    public CouponApiCreateRequestBuilder condition(CouponCondition condition) {
        this.condition = condition;
        return this;
    }

    public CouponApiCreateRequestBuilder createdBy(String createdBy) {
        this.createdBy = createdBy;
        return this;
    }

    public CouponApiCreateRequestBuilder discount(BigDecimal discount) {
        this.discount = discount;
        return this;
    }

    public CouponApiCreateRequestBuilder endDate(Instant endDate) {
        this.endDate = endDate;
        return this;
    }

    public CouponApiCreateRequestBuilder startDate(Instant startDate) {
        this.startDate = startDate;
        return this;
    }

    public CouponApiCreateRequestBuilder typeId(Integer typeId) {
        this.typeId = typeId;
        return this;
    }

    public CouponApiCreateRequestBuilder userId(Long userId) {
        this.userId = userId;
        return this;
    }

    public CouponApiCreateRequest build() {
        CouponApiCreateRequest couponApiCreateRequest = new CouponApiCreateRequest();
        couponApiCreateRequest.setCondition(condition);
        couponApiCreateRequest.setCreatedBy(createdBy);
        couponApiCreateRequest.setDiscount(discount);
        couponApiCreateRequest.setEndDate(endDate);
        couponApiCreateRequest.setStartDate(startDate);
        couponApiCreateRequest.setTypeId(typeId);
        couponApiCreateRequest.setUserId(userId);
        return couponApiCreateRequest;
    }
}

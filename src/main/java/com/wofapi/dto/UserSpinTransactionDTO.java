package com.wofapi.dto;

public class UserSpinTransactionDTO {

    private Boolean canSpin;

    private Long remainingTime;

    public UserSpinTransactionDTO() {
    }

    public UserSpinTransactionDTO(Boolean canSpin, Long remainingTime) {
        this.canSpin = canSpin;
        this.remainingTime = remainingTime;
    }

    public Boolean getCanSpin() {
        return canSpin;
    }

    public void setCanSpin(Boolean canSpin) {
        this.canSpin = canSpin;
    }

    public Long getRemainingTime() {
        return remainingTime;
    }

    public void setRemainingTime(Long remainingTime) {
        this.remainingTime = remainingTime;
    }

    @Override
    public String toString() {
        return "UserSpinTransactionDTO{" +
                "canSpin=" + canSpin +
                ", remainingTime=" + remainingTime +
                '}';
    }
}

package com.wofapi.dto;

import javax.validation.constraints.Min;
import java.math.BigDecimal;

public class CouponCondition {

    @Min(value = 0, message = "lowerLimit value must be equal or greater than zero")
    private BigDecimal lowerLimit;
    private Integer storefrontId;

    public CouponCondition() {

    }

    public CouponCondition(BigDecimal lowerLimit, Integer storefrontId) {
        this.lowerLimit = lowerLimit;
        this.storefrontId = storefrontId;
    }

    public BigDecimal getLowerLimit() {
        return lowerLimit;
    }

    public void setLowerLimit(BigDecimal lowerLimit) {
        this.lowerLimit = lowerLimit;
    }

    public Integer getStorefrontId() {
        return storefrontId;
    }

    public void setStorefrontId(Integer storefrontId) {
        this.storefrontId = storefrontId;
    }

    @Override
    public String toString() {
        return "CouponCondition{" +
                "lowerLimit=" + lowerLimit +
                ", storefrontId=" + storefrontId +
                '}';
    }
}

package com.wofapi.service;

import com.wofapi.common.RandomCouponCollection;
import com.wofapi.domain.WOFDetail;
import com.wofapi.dto.CouponCondition;
import com.wofapi.dto.CouponDTO;
import com.wofapi.dto.UserSpinTransactionDTO;
import com.wofapi.dto.request.CouponApiCreateRequest;
import com.wofapi.dto.request.CouponApiCreateRequestBuilder;
import com.wofapi.dto.request.WOFDetailCreateRequest;
import com.wofapi.model.AuthenticationContext;
import com.wofapi.repository.WOFRepository;
import com.wofapi.service.client.CouponApiClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Optional;

@Service
public class WOFDetailService {

    private static final Integer STORE_FRONT_ID = 1;
    private static final String CREATED_BY = "wof";

    private final WOFRepository wofRepository;
    private final CouponService couponService;
    private final CouponApiClient couponApiClient;
    private final AuthenticationService authenticationService;

    @Value("${expiration.coupon:2}")
    private Long couponExpiration;

    @Value("${expiration.campaign:24}")
    private Long campaignExpiration;

    public WOFDetailService(WOFRepository wofRepository,
                            CouponService couponService,
                            CouponApiClient couponApiClient,
                            AuthenticationService authenticationService) {
        this.wofRepository = wofRepository;
        this.couponService = couponService;
        this.couponApiClient = couponApiClient;
        this.authenticationService = authenticationService;
    }

    public void createWOFDetail(WOFDetailCreateRequest request) {
        CouponDTO coupon = couponService.getCouponById(request.getCouponId());

        if (!coupon.getDiscount().equals(BigDecimal.valueOf(0, 2))) {
            AuthenticationContext authenticationContext = authenticationService.authenticate();

            CouponCondition couponCondition = new CouponCondition(coupon.getLowerLimit(), STORE_FRONT_ID);
            CouponApiCreateRequest couponApiCreateRequest = CouponApiCreateRequestBuilder.couponApiCreateRequest()
                    .condition(couponCondition)
                    .discount(coupon.getDiscount())
                    .createdBy(CREATED_BY)
                    .startDate(Instant.now())
                    .endDate(getCouponExpirationInstant())
                    .typeId(0)
                    .userId(request.getUserId())
                    .build();

            couponApiClient.createCustomerCoupon(couponApiCreateRequest, authenticationContext.getAccessToken());
        }

        WOFDetail wofDetail = WOFDetail.createPersistedCouponWOFDetail(request.getUserId(),
                getCouponExpirationInstant(),
                request.getCouponId());
        wofRepository.save(wofDetail);
    }

    public CouponDTO spinWOF() {
        List<CouponDTO> availableCoupons = couponService.getAvailableCoupons();

        RandomCouponCollection couponCollection = new RandomCouponCollection();
        couponCollection.add(availableCoupons);

        return couponCollection.next();
    }

    public UserSpinTransactionDTO userLastSpinTransaction(Long userId) {
        Optional<WOFDetail> optionalWOFDetail = wofRepository.findTopByUserIdOrderByCreatedDateDesc(userId);

        if (optionalWOFDetail.isPresent()) {
            Duration between = Duration.between(optionalWOFDetail.get().getCreatedDate(), Instant.now());

            if (between.getSeconds() < getCampaignExpirationSeconds()) {
                return new UserSpinTransactionDTO(false,  getCampaignExpirationSeconds() - between.getSeconds());
            }
        }
        return new UserSpinTransactionDTO(true, 0L);

    }

    private Instant getCouponExpirationInstant() {
        return Instant.now().plusSeconds(getCouponExpirationSeconds());
    }

    private Long getCouponExpirationSeconds() {
        return couponExpiration * 60 * 60;
    }

    private Long getCampaignExpirationSeconds() {
        return campaignExpiration * 60 * 60;
    }

}

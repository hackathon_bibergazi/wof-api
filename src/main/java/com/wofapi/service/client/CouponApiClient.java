package com.wofapi.service.client;

import com.wofapi.dto.request.CouponApiCreateRequest;
import com.wofapi.dto.request.FormattedCouponApiCreateRequest;
import com.wofapi.exception.GenericRestClientException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

@Service
public class CouponApiClient {

    @Value("${coupon-api.host-url}")
    private String baseUrl;

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponApiClient.class);

    private final RestTemplate restTemplate;


    @Autowired
    public CouponApiClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public boolean createCustomerCoupon(CouponApiCreateRequest couponApiCreateRequest, String accessToken) {
        String uriString = UriComponentsBuilder
                .fromHttpUrl(baseUrl + "/coupons")
                .toUriString();
        HttpHeaders headers = prepareHttpHeaders(accessToken);
        HttpEntity<Object> httpEntity = new HttpEntity<>(formatDateParameters(couponApiCreateRequest), headers);

        try {
            restTemplate.postForEntity(uriString, httpEntity, Void.class);

            return true;
        } catch (GenericRestClientException e) {
            LOGGER.error("createCustomerCoupon has exception with request : {}",couponApiCreateRequest.toString() , e);
            throw e;
        }
    }

    private FormattedCouponApiCreateRequest formatDateParameters(CouponApiCreateRequest couponApiCreateRequest) {
        FormattedCouponApiCreateRequest formattedRequest = new FormattedCouponApiCreateRequest();

        String localStartDate = LocalDateTime.ofInstant(couponApiCreateRequest.getStartDate().plusSeconds(10800), ZoneOffset.UTC).toString();
        String localEndDate = LocalDateTime.ofInstant(couponApiCreateRequest.getEndDate().plusSeconds(10800), ZoneOffset.UTC).toString();

        formattedRequest.setCondition(couponApiCreateRequest.getCondition());
        formattedRequest.setCreatedBy(couponApiCreateRequest.getCreatedBy());
        formattedRequest.setDiscount(couponApiCreateRequest.getDiscount());
        formattedRequest.setTypeId(couponApiCreateRequest.getTypeId());
        formattedRequest.setUserId(couponApiCreateRequest.getUserId());
        formattedRequest.setEndDate(localEndDate);
        formattedRequest.setStartDate(localStartDate);

        return formattedRequest;


    }

    private HttpHeaders prepareHttpHeaders(String accessToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.setBearerAuth(accessToken);
        headers.set("x-storefrontid", "wof");
        headers.set("x-applicationid", "1");
        headers.set("x-correlationid", UUID.randomUUID().toString());
        headers.set("x-storefrontid", "1");
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }


}

package com.wofapi.service.client;

import com.wofapi.dto.AuthenticationResponseDTO;
import com.wofapi.dto.UserResponseDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class BackendUserApiClient {

    @Value("${backend-user-api.host-url}")
    private String baseUrl;

    private static final Logger LOGGER = LoggerFactory.getLogger(BackendUserApiClient.class);

    private final RestTemplate restTemplate;

    public BackendUserApiClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public UserResponseDto getUserByEmail(String email) {

        String url = "/users?loweredUserName=";
        String productUrlByBarcode = baseUrl + url + email;

        HttpHeaders headers = prepareHttpHeaders();
        try {
            List<UserResponseDto> userResponseDtos = restTemplate.exchange(
                    productUrlByBarcode,
                    HttpMethod.GET,
                    new HttpEntity<>(null, headers),
                    new ParameterizedTypeReference<List<UserResponseDto>>() {
                    }
            ).getBody();
            return userResponseDtos.get(0);
        } catch (Exception e) {
            LOGGER.error("getUserByEmail has exception with email : {}", email );
            throw e;
        }
    }

    private HttpHeaders prepareHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }
}

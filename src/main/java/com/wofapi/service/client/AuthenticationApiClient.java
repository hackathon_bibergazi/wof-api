package com.wofapi.service.client;

import com.wofapi.dto.AuthenticationResponseDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;

@Service
public class AuthenticationApiClient {

    @Value("${authentication-api.host-url}")
    private String baseUrl;

    private final RestTemplate restTemplate;

    public AuthenticationApiClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public AuthenticationResponseDTO authenticate(MultiValueMap<String, String> requestMap) {
        HttpHeaders headers = prepareHttpHeaders();

        return restTemplate
                .postForEntity(
                        baseUrl + "/oauth2/token",
                        new HttpEntity<>(requestMap, headers),
                        AuthenticationResponseDTO.class).getBody();
    }

    private HttpHeaders prepareHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        return headers;
    }
}

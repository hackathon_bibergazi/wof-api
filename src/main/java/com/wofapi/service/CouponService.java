package com.wofapi.service;

import com.wofapi.domain.Coupon;
import com.wofapi.dto.CouponDTO;
import com.wofapi.dto.request.CouponCreateRequest;
import com.wofapi.dto.request.DescriptionsAddRequest;
import com.wofapi.dto.request.DescriptionsRemoveRequest;
import com.wofapi.repository.CouponRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CouponService {

    private final CouponRepository couponRepository;

    public CouponService(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    public void createCoupon(CouponCreateRequest request) {
        Coupon coupon = Coupon.createCoupon(request.getDiscount(), request.getProbability(), request.getLowerLimit());
        couponRepository.save(coupon);
    }

    public CouponDTO getCouponById(Long id) {
        return convert(couponRepository.findOne(id));
    }

    public List<CouponDTO> getAvailableCoupons() {
        return couponRepository.findActivatedCoupons()
                .stream()
                .map(this::convert)
                .collect(Collectors.toList());
    }

    public void deactivateAvailableCoupon(Long couponId) {
        Coupon coupon = couponRepository.findActivatedOne(couponId);

        coupon.deactivate();
        couponRepository.save(coupon);
    }

    public void activateAvailableCoupon(Long couponId) {
        Coupon coupon = couponRepository.findDeactivatedOne(couponId);

        coupon.activated();
        couponRepository.save(coupon);
    }

    public void addDescriptions(DescriptionsAddRequest request) {
        Coupon coupon = couponRepository.findOne(request.getCouponId());

        coupon.addDescriptions(request.getDescriptions());
        couponRepository.save(coupon);
    }

    public void removeDescriptions(DescriptionsRemoveRequest request) {
        Coupon coupon = couponRepository.findOne(request.getCouponId());

        coupon.removeDescriptions(request.getDescriptions());
        couponRepository.save(coupon);
    }

    private CouponDTO convert(Coupon coupon) {
        return new CouponDTO(coupon.getId(),
                coupon.getDiscount(),
                coupon.getProbability(),
                coupon.getLowerLimit(),
                coupon.getCapability(),
                coupon.getDescriptions(),
                coupon.isActivated());
    }

}

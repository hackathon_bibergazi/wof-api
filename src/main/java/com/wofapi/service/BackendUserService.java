package com.wofapi.service;

import com.wofapi.dto.UserResponseDto;
import com.wofapi.service.client.BackendUserApiClient;
import com.wofapi.service.client.CouponApiClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class BackendUserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BackendUserService.class);

    private final BackendUserApiClient backendUserApiClient;

    public BackendUserService(BackendUserApiClient backendUserApiClient) {
        this.backendUserApiClient = backendUserApiClient;
    }

    public UserResponseDto getUserByEmail(String email) {
        return backendUserApiClient.getUserByEmail(email);
    }
}

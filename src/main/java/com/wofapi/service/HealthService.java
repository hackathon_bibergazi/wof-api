package com.wofapi.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class HealthService {

    @Value("${unhealthy.waiting.time.in.millis:15000}")
    private int millis;

    private boolean healthy = true;

    public boolean isHealthy() {
        return healthy;
    }

    public void setHealthy(boolean healthy) {
        this.healthy = healthy;
    }

    public void unhealthy() throws InterruptedException {
        setHealthy(false);
        Thread.sleep(millis);
    }

}


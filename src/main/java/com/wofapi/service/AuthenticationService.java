package com.wofapi.service;

import com.wofapi.dto.AuthenticationResponseDTO;
import com.wofapi.model.AuthenticationContext;
import com.wofapi.service.client.AuthenticationApiClient;
import com.wofapi.util.Clock;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Date;

@Service
public class AuthenticationService {

    private static final int TOKEN_EXPIRE_BUFFER = 60;

    @Value("${authentication.client-id}")
    private String clientId;

    @Value("${authentication.grant-type}")
    private String grantType;

    private final AuthenticationApiClient authenticationApiClient;

    private final AuthenticationContext authenticationContext;


    public AuthenticationService(AuthenticationApiClient authenticationApiClient, AuthenticationContext authenticationContext) {
        this.authenticationApiClient = authenticationApiClient;
        this.authenticationContext = authenticationContext;
    }

    public AuthenticationContext authenticate() {
        if (authenticationContext.getExpireDate() == null || authenticationContext.getExpireDate().before(Clock.now().toDate())) {
            MultiValueMap<String, String> request = prepareAuthenticationRequest();
            AuthenticationResponseDTO authenticationResponse = authenticationApiClient.authenticate(request);
            authenticationResponse.setExpiresIn(authenticationResponse.getExpiresIn() - TOKEN_EXPIRE_BUFFER);
            setAuthenticationToContext(authenticationResponse);
        }
        return authenticationContext;
    }

    private void setAuthenticationToContext(AuthenticationResponseDTO authenticationResponse) {
        DateTime now = Clock.now();
        Date expireDate = now.plus(((long) authenticationResponse.getExpiresIn()) * 1000L).toDate();

        authenticationContext.setAccessToken(authenticationResponse.getAccessToken());
        authenticationContext.setType(authenticationResponse.getTokenType());
        authenticationContext.setExpireDate(expireDate);
    }


    private MultiValueMap<String, String> prepareAuthenticationRequest() {
        MultiValueMap<String, String> requestMap = new LinkedMultiValueMap<>();
        requestMap.add("client_id", clientId);
        requestMap.add("grant_type", grantType);
        return requestMap;
    }



}
